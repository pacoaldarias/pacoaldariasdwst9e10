<?php
if (isset($_REQUEST["estaciones"])) {
  // API Key requerida para acceso a APIREST OpenData
  $apikey = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwYWNvLmFsZGFyaWFzQGNlZWRjdi5lcyIsImp0aSI6ImIxMTA2OWEzLWIxNzItNGVjYS1hNDU4LWM2MjI3YTNkOGJmNiIsImlzcyI6IkFFTUVUIiwiaWF0IjoxNTgwNDIzMjc0LCJ1c2VySWQiOiJiMTEwNjlhMy1iMTcyLTRlY2EtYTQ1OC1jNjIyN2EzZDhiZjYiLCJyb2xlIjoiIn0.vAkCFshfE5qe3DQv2nqGG6-34AVcZzdf0XkXoa7q2ck";
  $codEstacion = $_REQUEST["estaciones"]; // recogemos valor de select con el id de la estación metereológica

  /**
   * Función que devuleve la url de donde proceden los datos de AEMT
   *
   * @param [int] $codEstacion
   * @param [string] $apikey
   * @return [string] $respuesta Devuelve la URL de AEMET
   */
  function API($codEstacion, $apikey) {
    $url = "https://opendata.aemet.es/opendata/api/observacion/convencional/datos/estacion/";
    $respuesta = $url . $codEstacion . "/?api_key=" . $apikey;
    return $respuesta;
  }

  $fuente = API($codEstacion, $apikey); // llamada a función
  $json = file_get_contents($fuente); // guardamos datos en formato json
  $datos = json_decode($json, true); // decodificamos datos y transformamos json a array

  error_reporting(0);
  $fuenteDatos = $datos["datos"]; // obtenemos url con datos de la estación metereológica
  $metaDatos = $datos["metadatos"]; // obtenemos los metadatos de los campos de la información
  // convertimos la información a formato json
  $json_fuentedatos = file_get_contents($fuenteDatos);
  $json_metadatos = file_get_contents($metadatos);
  // decodificamos información pasando json a array
  $datos_fuenteDatos = json_decode($json_fuentedatos, true);
  $datos_metadatos = json_decode($json_metadatos, true);
}
?>

<html>

    <body>

        <h2>Datos de observaciones convencionales AEMET.</h2>
        <h3>Último informe elaborado de 24 h.</h3>
        <h4>Provincia de Valencia</h4>

        <form action="Aemet.php" method="get">
            <select name="estaciones" id="estaciones">
                <option value="">----Seleccione una estación----</option>
                <option value="8381X">Ademuz</option>
                <option value="8072Y">Barx</option>
                <option value="8270X">Bicorp</option>
                <option value="8319X">Buñol</option>
                <option value="8300X">Carcaixent</option>
                <option value="8395X">Chelva</option>
                <option value="8290X">Enguera-Navalón</option>
                <option value="8193E">Jalance</option>
                <option value="8409X">Liria</option>
                <option value="8058Y">Miramar</option>
                <option value="8058X">Oliva</option>
                <option value="8283X">Ontinyent</option>
                <option value="8325X">Polinyà del Xúquer</option>
                <option value="8446Y">Sagunto</option>
                <option value="8337X">Turís</option>
                <option value="8309X">Utiel</option>
                <option value="8416Y">Valencia</option>
                <option value="8414A">Valencia-Aeropuerto</option>
                <option value="8293X">Xàtiva</option>
                <option value="8203O">Zarra</option>
            </select>
            <button type="submit">Solicitar</button>
        </form>
        <div id="mostrarDatos">
            <ul>
                <?php
                if (isset($_REQUEST["estaciones"])) { // si se ha realizado submit
                  // si no hay datos
                  if ($datos_fuenteDatos == null) { // imprimimos mensaje
                    echo "Datos no disponibles en este momento. Inténtelo más tarde.";
                  } else { // si hay datos
                    // obtenemos los registros de la estación metereológica
                    // guardamos los valores en variables
                    foreach ($datos_fuenteDatos as $key => $value) {
                      $codigo_estacion = $value["idema"];
                      $nombre_estacion = $value["ubi"];
                      $longitud = $value["lon"];
                      $latitud = $value["lat"];
                      $altitud = $value["alt"];
                      $rango_hora = $value["fint"];
                      $prec_acumulada = (isset($value["prec"]) && $value["prec"] != "") ? $value["prec"] . " l/m2" : "Sin predicción";
                      $prec_liq_acumulada = (isset($value["pliqtp"]) && $value["pliqtp"] != "") ? $value["pliqtp"] . " l/m2" : "Sin predicción";
                      $prec_sol_acumulada = (isset($value["psolt"]) && $value["psolt"]) ? $value["psolt"] . " l/m2" : "Sin predicción";
                      $vel_max_viento = (isset($value["vmax"]) && $value["vmax"] != "") ? $value["vmax"] . " m/s" : "Sin predicción";
                      $vel_md_viento = (isset($value["vv"]) && $value["vv"] != "") ? $value["vv"] . " m/s" : "Sin predicción";
                      $dir_md_viento = (isset($value["dv"]) && $value["dv"] != "") ? $value["dv"] . " m/s" : "Sin predicción";
                      $humedad_relativa = (isset($value["hr"]) && $value["hr"] != "") ? $value["hr"] . " %" : "Sin predicción";
                      $presion_atmosferica = (isset($value["pres"]) && $value["pres"] != "0") ? $value["pres"] . " hPr" : "Sin predicción";
                      $temperatura = (isset($value["ts"]) && $value["ts"] != "") ? $value["ts"] . " °" : "Sin predicción";
                      $temperatura_aire = (isset($value["ta"]) && $value["ta"] != "") ? $value["ta"] . " °" : "Sin predicción";
                      $nieve = (isset($value["nieve"]) && $value["nieve"] != "") ? $value["nieve"] . " cm" : "Sin predicción";

                      // generamos html para mostar los datos en el DOM
                      echo "
                        <li>
                            <h1>$codigo_estacion - $nombre_estacion</h1>
                            <h2>Latitud: $latitud ° - Longitud: $longitud ° , Altitud: $altitud m</h2>
                            <h3>Tramo horario: $rango_hora</h3>
                            <ul>
                                <li>Temperatura Suelo: $temperatura</li>
                                <li>Temperatura Aire: $temperatura_aire</li>
                            </ul>
                            <ul>
                                <li>Velocidad Max. del Viento: $vel_max_viento</li>
                                <li>Velocidad Med. del Viento: $vel_md_viento</li>
                                <li>Dirección del Viento: $dir_md_viento </li>
                            </ul>
                            <ul>
                                <li>Precipitación Acumulada: $prec_acumulada</li>
                                <li>Precipitación Líquida Acumulada: $prec_liq_acumulada</li>
                                <li>Precipitación Sólida Acumulada: $prec_sol_acumulada</li>
                            </ul>
                            <ul>
                                <li>Humedad Relativa: $humedad_relativa</li>
                            </ul>
                            <ul>
                                <li>Presión Atmosférica: $presion_atmosferica</li>
                            </ul>
                            <ul>
                                <li>Espesor de nieve: $nieve</li>
                            </ul>
                        </li>
                    ";
                    }
                  }
                }
                ?>
            </ul>
        </div>


    </body>
</html>